# Keelah Voice Assistant

[TOC]

---

## Purpose

TBD

---

## Usage

TBD

---

## Building and Testing Locally

This project uses both [Fastlane](https://fastlane.tools/) and [Gradle](https://gradle.org/) and to handle building, testing and so on.  Fastlane wraps Gradle commands to handle the building process since there is a a Gradle Android plugin that makes compiling for Android projects incredibly simple.  The CI/CD pipeline handles publishing (if needed), so you do not have to use Fastlane if you do not wish to.  Both methods of running the build locally are documented below, so feel free to pick whichever suites your fancy.

A full, in-depth explanation of the entire build architecture is available on the [Confluence Wiki](https://venkee-enterprises.atlassian.net/wiki/spaces/KEELAH/pages/749568001/Android+Build+Process+And+Architecturehttps://docs.gitlab.com/ee/ci/) if you wish to read more into _how_ all of this works.

### Fastlane

TBD

### Gradle

You do not have to have the Gradle command-line tool installed locally to run Gradle commands.  You can use the [gradlew](gradlew) (for Mac or Linux) or [gradlew.bat](gradlew.bat) (for Windows) to run Gradle commands without needing to install it onto your machine.  You can use the `gradlew` wrapper just like you would any other command-line tool by just specifying the path to the tool and passing any arguments to it:

Using a locally installed Gradle: `gradle <some task>`
Using the wrapper from the repo: `<path to repo>/gradlew <some task>`

Gradle "tasks" are used to run the compilation and testing, but the configurations for these tasks are done in the package-level [build.gradle](app/build.gradle) file.  Most of the tasks are defined by the [Android Gradle plug-in](http://android-doc.github.io/tools/building/plugin-for-gradle.html) and so are explicitly defined in the repo.  Below are the commands you would use to build or test your code.  Simply run Gradle and specify the task you wish to run:

* `gradle assembleDebug` → Compiles a debug variant.
* `gradle assembledRelease` → Compiles a release variant.
* `gradle test` → Runs any unit and integration tests.

---

## CI/CD

[GitLab CI](https://docs.gitlab.com/ee/ci/) is used to handle building the code for pull requests and performing releases as needed.  The [.gitlab-ci.yml](.gitlab-ci.yml) contains all of the steps to create a Docker image, build the code, run unit tests, and so on in a pipeline.  The `.gitlab-ci.yml` file is well-documented, so please see that file and the [Confluence wiki](https://venkee-enterprises.atlassian.net/wiki/spaces/KEELAH/pages/749568001/Android+Build+Process+And+Architecture) for more in-depth information.

---

## Important Files

Due to the complex nature of building Android projects, there are a lot of extra files that are not source code within this project.  The below list provides a brief description as to the purpose of these files so as to alleviate any confusion:

* [.gitignore](.gitignore) → Specified files that should be ignored by Git, such as build artifacts, when checking in files to source control.
* [.gitlab-ci.yml](.gitlab-ci.yml) → Provides a pipeline definition for running GitLab CI/CD pipelines.
* [Dockerfile](Dockerfile) → Provides a way to build the app in a CI/CD cloud environment by using a Docker image.
* [fastlane](fastlane) → All files related to configuring the [Fastlane](https://fastlane.tools/) app automation tool.
* [Gemfile](Gemfile) and [Gemfile.lock](Gemfile.lock) → These are used by [Ruby's package manager](https://guides.rubygems.org/what-is-a-gem/) to install Fastlane and any of it's dependencies.
* [build.gradle](build.gradle) → Configures global Gradle build definitions, like which plugins to use and where to fetch dependencies.
* Any module-level Gradle files, such as the one in [app/build.gradle](app/build.gradle) → Handles each module's build settings, such as the Android SDK version, run time dependencies, test dependencies, and Android package names.
* [gradle.properties](gradle.properties) → Since Gradle is Java-based tool, this file provides a way to Java properties or command-line flags to the execution of Gradle itself.
* [setting.gradle](settings.gradle) → Any project-level configurations for the [build.gradle](build.gradle) such as multi-project builds.
* [gradlew](gradlew) and [gradlew.bat](gradlew.bat) → Wrapper files to allow running Gradle command on Linux or Windows command lines without needing to install the tool.
* [gradle/wrapper](gradle/wrapper/) → The actual JAR file containing the Gradle tool for using with `gradlew`.

---
